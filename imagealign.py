'''
Copyright (C) 2017 Matyas Kocsis, matyilona@openmailbox.org

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
barraud@math.univ-lille1.fr

Extension for aligning multiple bitmap images, by selecting the same points on them.
'''

# These two lines are only needed if you don't put the script directly into
# the installation directory
import sys
sys.path.append( '/usr/share/inkscape/extensions' )

import inkex

import simplestyle
import simpletransform
import numpy as np
import math
import logging

#a logger for debugging/warnings
logger = logging.getLogger( "imagelign" )
logger.setLevel( 'WARNING' )
sh = logging.StreamHandler( sys.stderr )
sh.setLevel( logging.DEBUG )
logger.addHandler( sh )
logger.debug( "Logger initialized" )

class ImageAlignEffect( inkex.Effect ):
    """
    Either draw marks for alignment, or align images
    """
    def __init__( self ):
        """
        Setting stuff up
        """
        inkex.Effect.__init__( self )

        self.OptionParser.add_option( '-s', '--start', action = 'store',
          type = 'string', dest = 'start', default = 'start',
          help = 'Start for inserting align marks, end for alignment' )
        self.OptionParser.add_option( '-o', '--od', action = 'store',
          type = 'string', dest = 'alignid', default = 'alignimage',
          help = 'Id of object to be aligned (use groups for multipe)' )
        self.OptionParser.add_option( '-n', '--markernum', action = 'store',
          type = 'int', dest = 'markernum', default = '1',
          help = 'No. of images to align' )

    color_list = [ '#0000ff', '#00ff00', '#ff0000', '#ff00ff', '#00ffff', '#ffff00', '#8800ff', '#0088ff', '#00ff88', '#ff0088' ] 

    def getMarker( self, ind1, ind2 ):
        """
        Make a marker ind1: index of image to align, ind2: number of marker
        every marker is a group with an arrow and a label
        """
        #create a group element to place the text and arrow in
        group = inkex.etree.Element( 'g' )
        group.set( 'id', 'alignmarkgroup%i%i' % ( ind1, ind2 ) )
        #style for both the arrow and the label
        style = { 'stroke' : 'none', 'stroke-width' : '0', 'fill-opacity' : '.5', 'fill' : self.color_list[ ind1 ] }
        style = simplestyle.formatStyle( style )
        #path for the arrow
        arrow_path = 'm 0,0 0,5 5,0 z'
        inkex.etree.SubElement( group, 'path', { 'style' : style, 'd' : arrow_path } ).set( 'id', 'alignmark%i%i' % ( ind1, ind2 ) )
        inkex.etree.SubElement( group, 'text', { 'style' : style } ).text = str( ind2 )
        return( group )

    def cleanup( self ):
        """
        Delet previous align layers and markers
        """
        la = self.getElementById( "alignlayera" )
        lb = self.getElementById( "alignlayerb" )
        if la is not None:
            la.getparent().remove(la)
        if lb is not None:
            lb.getparent().remove(lb)

        #some marks might have been moved to other layers
        for i in range( 3 ):
            for j in range( 1, self.options.markernum + 1 ):
                marker_group = self.getElementById( "alignmarkgroup%i%i" % (i, j) )
                if marker_group is not None:
                    marker_group.getparent().remove( marker_group )


    def putMarkers( self ):
        """
        Put 3 markers on svg, for every image, and 3 for target
        """
        #delet old marks
        self.cleanup()
        #set up layers for markers
        svg = self.document.getroot()
        layera = inkex.etree.SubElement( svg, "g" )
        layera.set( 'id', 'alignlayera' )
        layera.set( inkex.addNS( "label", "inkscape" ), "Align Layer A" )
        layera.set( inkex.addNS( "groupmode", "inkscape" ), "layer" )
        layerb = inkex.etree.SubElement( svg, "g" )
        layerb.set( 'id', 'alignlayerb' )
        layerb.set( inkex.addNS( "label", "inkscape" ), "Align Layer B" )
        layerb.set( inkex.addNS( "groupmode", "inkscape" ), "layer" )
        #draw markers
        for i in range( 3 ):
            ma = self.getMarker( 0, i )
            ma.set( 'transform', 'translate(%i  0)' % 20 * i )
            layera.append( ma )
            for j in range( 1, self.options.markernum + 1 ):
                mb = self.getMarker( j, i )
                mb.set( 'transform', 'translate(%i %i)' % ( i*20, j*30 ) )
                layerb.append( mb )

    def alignImage( self ):
        """
        Align image based on the real backtransformed coords of the markers
        """
        points = { }
        for i in range( self.options.markernum + 1):
            points[ i ] = [ ]
            for j in range( 3 ):
                r = self.getElementById( 'alignmark%i%i' % ( i, j ) )
                #get the point marker points to from boundingbox's corner
                if r is not None:
                    bb = simpletransform.computeBBox( [r] )
                else:
                    raise( BaseException( "Can't find alignmark%i%i" % ( i, j ) ) )
                x = bb[0]
                y = bb[3]
                #use 3x3 matrix convention, numpy wants that, simleTransform honeybadger
                point = [ x, y, 1 ]
                #get transforms on point
                a = simpletransform.composeParents( r, simpletransform.parseTransform( None ) ) 
                #apply to stored coords not real etree object
                simpletransform.applyTransformToPoint( a , point )
                points[i].append( point )
            
            if i != 0:
                #get 3x3 numpy transform matrix
                a = np.matrix( points[ 0 ], dtype = float ).getT()
                b = np.matrix( points[ i ], dtype = float ).getT()
                t = ( a * b.getI() )
                for j in range( 3 ):
                    r = self.getElementById( 'alignmarkgroup%i%i' % ( i, j ) )
                    #transform markes 
                    simpletransform.applyTransformToNode( t.tolist(), r )
                #get the image we're working on
                img = self.getElementById( self.options.alignid + str( i ) )
                if img is not None:
                    #parents transform will always get apply after nodes own, but t should be applied to already transformed coordinates
                    u = simpletransform.composeParents( img, simpletransform.parseTransform( None ) ) + [[ 0, 0, 1 ]]
                    u = np.matrix( u, dtype = float )
                    v =  u.getI() * t * u
                    simpletransform.applyTransformToNode( v.tolist() , img )
                else:
                    logger.warning( "Could not find %s" % self.options.alignid + str( i ) )

    def effect( self ):
        """
        Effect behaviour.
        Overrides base class method.
        """
        logger.debug( "Effecting started" )
        if self.options.start == 'start':
            self.putMarkers( )
        else:
            self.alignImage( )



# Create effect instance and apply it.
logger.debug( "Started with: {}.".format( str( sys.argv ) ) )
effect = ImageAlignEffect()
effect.affect()
